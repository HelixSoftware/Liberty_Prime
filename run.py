from __future__ import print_function
import os
import sys
import time
import logging
import tempfile
import colorlog
import traceback
import subprocess
from time import gmtime, strftime

tmpfile = tempfile.TemporaryFile('w+', encoding='utf8')
log = logging.getLogger('launcher')
log.setLevel(logging.DEBUG)

sh = logging.StreamHandler(stream=sys.stdout)
sh.setFormatter(logging.Formatter(
    fmt="[%(levelname)s] %(name)s: %(message)s"
))

sh.setLevel(logging.INFO)
log.addHandler(sh)

tfh = logging.StreamHandler(stream=tmpfile)
tfh.setFormatter(logging.Formatter(
    fmt="[%(relativeCreated).9f] %(asctime)s - %(levelname)s - %(name)s: %(message)s"
))
tfh.setLevel(logging.DEBUG)
log.addHandler(tfh)


def finalize_logging():
    if not os.path.exists("logs"):
        log.info("logging folder doesnt exist")
        os.mkdir("logs")
    if os.path.isfile("logs/bot.log"):
        log.info("Moving old bot log")
        try:
            if not os.path.exists("logs/archive"):
                log.info("archive folder doesnt exist. creating")
                os.mkdir("logs/archive")
            if os.path.exists("logs/archive"):
                name = "logs/archive/" + (str(strftime("%Y-%m-%d  %H-%M-%S", gmtime()))) + ".log"
                if os.path.isfile(name):
                    time.sleep(2)
                    name = "log/archive/" + (str(strftime("%Y-%m-%d  %H:%M:%S", gmtime()))) + ".log"
                os.rename("logs/bot.log", name)
        except Exception as e:
            log.critical("unable to create logging file")
            log.critical(e)

    with open("logs/bot.log", 'w', encoding='utf8') as f:
        tmpfile.seek(0)
        f.write(tmpfile.read())
        tmpfile.close()

        f.write('\n')
        f.write(" PRE-RUN SANITY CHECKS PASSED ".center(80, '#'))
        f.write('\n\n\n')

    global tfh
    log.removeHandler(tfh)
    del tfh

    fh = logging.FileHandler("logs/bot.log", mode='a')
    fh.setFormatter(logging.Formatter(
        fmt="[%(relativeCreated).9f] %(name)s-%(levelname)s: %(message)s"
    ))
    fh.setLevel(logging.DEBUG)
    log.addHandler(fh)

    sh.setLevel(logging.INFO)

    dlog = logging.getLogger('discord')
    dlh = logging.StreamHandler(stream=sys.stdout)
    dlh.terminator = ''
    dlh.setFormatter(logging.Formatter('.'))
    dlog.addHandler(dlh)
    log.info("logging setup completed")

def checks():
    if not os.path.exists("code"):
        log.error("Code directory does not exist")
        os.mkdir("code")
        checks = False
    if not os.path.exists("data"):
        log.error("Data directory does not exist")
        os.mkdir("data")
        log.error("created")
    checks = True
    try:
        finalize_logging()
    except:
        checks = False
    return checks

def main():
    from code import Liberty
    L = Liberty()

    L.boot()

    log.info("All done.")


if __name__ == '__main__':
    if checks() == True:
        os.system("chcp 65001")
        main()
    else:
        log.critical("Checks failed, aborting")
