import os
import asyncio
import datetime
import inspect
import json
import logging
import random
import sys
import tempfile
import timeit
import unicodedata
import aiohttp
import discord
import requests
from fuzzywuzzy import fuzz
from time import strftime, gmtime
from collections import defaultdict

#import util functions
import code.get as get


tmpfile = tempfile.TemporaryFile('w+', encoding='utf8')
log = logging.getLogger("Liberty runtime")


class Liberty(discord.Client):
#
#
# Functions
    def __init__(self):
        self.shard_count = 1
        self.shard_id = 0
        self.errors = int(0)
        self.version = "0.50"
        try:
            self.token = str(open("data/token.store", "r").read())
        except:
            self.token = "INVALID"
        self.owner = ""
        self.prefix = "L-"
        self.boottime = ""
        self.logintime = ""
        self.statusmsg = ""
        self.violations = int(0)
        self.process = int(0)
        self.messagedict = defaultdict(dict)
        try:
            spammers = open('spammer.txt','r').read()
            spammers = spammers.splitlines()
            self.spammers = spammers
        except:
            self.spammers = ""
        super().__init__()
        self._setup_logging()
        self.aiosession = aiohttp.ClientSession(loop=self.loop)

    def boot(self):
        loop = asyncio.get_event_loop()
        self.loop = loop
        try:
            loop.run_until_complete(self.start(self.token))
        except discord.LoginFailure:
            log.error("Invalid login token")
            token = input(log.info("What is the token? "))
            log.info("Applying new token")
            token = token.replace("\n", "")
            token = token.replace(" ", "")
            target = open("data/token.store", "w")
            target.writelines(token)
            target.close()
            os.execl(sys.executable, sys.executable, *sys.argv)
        except Exception as e:
            self.errors += 1
            log.error(e)
            log.info("logging out")
            loop.run_until_complete(self.logout())
        finally:
            try:
                pending = asyncio.Task.all_tasks()
                gathered = asyncio.gather(*pending)

                try:
                    gathered.cancel()
                    loop.run_until_complete(gathered)
                    gathered.exception()
                except:
                    self.errors += 1
                log.info("Logged out")
                log.info(("Session Errors: " + str(self.errors)))
            except Exception as e:
                self.errors += 1
                log.error(("Error in cleanup:", e))
                log.info("\n\n")
                log.info("Logged out")
                log.info(("Session Errors: " + str(self.errors)))
            loop.close()

    def _setup_logging(self):
        logging.basicConfig(format='[%(name)s//%(funcName)s]  -  %(message)s', level=logging.INFO)
        handler = logging.FileHandler(filename='logs/bot.log', encoding='utf-8', mode='w')
        handler.setFormatter(logging.Formatter('%(asctime)s:   %(name)s//%(funcName)s:   %(message)s'))
        log.addHandler(handler)
        logging.getLogger("discord").setLevel(logging.WARNING)
        logging.getLogger("websockets").setLevel(logging.WARNING)

    async def reportError(self, error, location):
        try:
            await self.send_message(discord.Object("301010721037090818"), "**Error:**\n\n{}\n{}".format(str(error.__str__()), str(location)))
            await self.send_message(self.owner, "**Error:**\n\n{}\n{}".format(str(error.__str__()), str(location)))
        except:
            pass
        log.error("{}\n{}".format(str(error.__str__()), str(location)))

    async def blacklistChecks(self, word, item, message):
        channel = message.channel
        if len(item) <= 5:
            prob = 100
        elif len(item) == 6:
            prob = 90
        elif len(item) <= 9:
            prob = 90
        elif len(item) > 10:
            prob = 70
        else:
            log.warning(len(item))
            prob = 80
        punc = "*!£$%^&*()_-+={[}]:;@'~#<,>.?/|\`¬\"¡ # $ % ⅋ , ¿"
        char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        smallcaps = "ᴀʙᴄᴅᴇғɢʜɪᴊᴋʟᴍɴᴏᴘǫʀsᴛᴜᴠᴡxʏᴢᴀʙᴄᴅᴇғɢʜɪᴊᴋʟᴍɴᴏᴘǫʀsᴛᴜᴠᴡxʏᴢ"
        superscript = "ᵃᵇᶜᵈᵉᶠᵍʰᶦʲᵏˡᵐⁿᵒᵖᑫʳˢᵗᵘᵛʷˣʸᶻᴬᴮᶜᴰᴱᶠᴳᴴᴵᴶᴷᴸᴹᴺᴼᴾQᴿˢᵀᵁⱽᵂˣʸᶻ"
        upsidedown = "ɐqɔpǝɟƃɥᴉɾʞlɯuodbɹsʇnʌʍxʎz∀qƆpƎℲפHIſʞ˥WNOԀQɹS┴∩ΛMX⅄Z"
        fullwidth = "ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ"
        smallcaps = str.maketrans(smallcaps, char)
        superscript = str.maketrans(superscript, char)
        upsidedown = str.maketrans(upsidedown, char)
        fullwidth = str.maketrans(fullwidth, char)
        attempts = []
        attempts.append(word)
        attempts.append(word.translate(smallcaps))
        attempts.append(word.translate(superscript))
        upsideword = word.translate(upsidedown)
        attempts.append(upsideword[::-1])
        attempts.append(word.translate(fullwidth))
        unicode = str(unicodedata.normalize('NFKD', word).encode('ascii', 'ignore'))
        unicode = unicode.replace("'", "")
        unicode = unicode[1:]
        for i in range(len(punc) - 1):
            unicode = unicode.replace(punc[i], "")
        unicode = unicode.lower()
        attempts.append(unicode)
        for attempt in attempts:
            if fuzz.ratio(attempt, item) >= prob:
                log.info("{} said {}. Match probability: {}%".format(
                    message.author.display_name, item, str(fuzz.ratio(attempt, item))))
                await self.send_message(channel, (
                    "BLACKLISTED WORD FROM {}... {}% MATCH".format(message.author.display_name,
                                                                   str(fuzz.ratio(attempt, item)))))
                await self.delete_message(message)
                self.violations += 1
                # await self.logEvent(event="Deleted Blacklisted word",
                #               details="{} said {}".format(message.author.name, item),
                #               channel=message.channel, server=message.server, user=message.author)
                return True

    async def checkMSG(self, message):
        channel = message.channel
        dir = "data/" + message.server.id + ".json"
        if not os.path.exists("data"):
            os.mkdir("data")
        if not os.path.isfile(dir):
            return
        else:
            with open(dir, 'r') as r:
                data = json.load(r)
                prefix = str(data["prefix"])
                everyoneMentionBlock = str(data["mention"])
                welcomeChannel = str(data["welcome"])
                announcementChannel = str(data["announcement"])
                announcementChannel = discord.Object(announcementChannel)
                urlBlock = str(data["urlblock"])
                try:
                    punnishment = str(data["punnishment"])
                except:
                    punnishment = None
                try:
                    blacklist = list(data['blacklist'])
                except:
                    self.errors += 1
                    blacklist = False
            if channel == announcementChannel:
                return
        self.process += 1
        msg = message.content.strip()

        #Liberty got mentioned
        if self.user in message.mentions:
            log.info(str("LIBERTY PRIME WAS MENTIONED: " + str(message.author.name) + '  "' + msg + '"'))
            if not message.author.bot:
                if len(msg) != 21 and len(msg)!= 22:
                    if "inspire me" in msg:
                        await self.inspire(channel)
                        return
                    if message.author.id == "174918559539920897" and "debug" in msg.lower():
                        mesg = "**BOT OWNER DEBUG MODE ACTIVE**\n\nPrefix = {}\nMentioning = {}\nAnnouncements = <#{}>\nWelcome = <#{}>\nURL Blocking = {}\nBlacklisted words = {}\nServer Owner = <@{}>".format(
                            str(prefix), str(everyoneMentionBlock), str(announcementChannel.id), str(welcomeChannel), str(urlBlock), str(blacklist), str(message.server.owner.id)
                        )
                        await self.send_message(channel, mesg)
                        return
                else:
                    await self.cmd_help(message.channel)
                    return

        if message.author == self.user\
                or message.author == channel.server.owner\
                or message.author == self.owner \
                or message.channel == announcementChannel \
                or message.channel == welcomeChannel:
            return
        messagelist = list(self.messagedict[message.server.id][message.author.id][message.author.id])
        last4 = messagelist[-4:]
        if len(last4) < 4:
            pass
        else:
            if last4[0].content == last4[1].content == last4[2].content == last4[3].content:
                await self.send_message(message.channel, "SPAM DETECTED FROM {}".format(message.author.display_name))
                if punnishment == "kick":
                    await self.kick(message.author)
                elif punnishment == "ban":
                    await self.ban(message.author)
                elif punnishment == "mute":
                    await self.send_message(c, "INCOMPLETE FUNCITON...")
                else:
                    await self.send_message(message.channel, "NO PUNNISHMENT SET, SET ONE WITH ``{}SETUP`` OR ``{}PUNNISH``".format(prefix, prefix))
                for message in last4:
                    await self.delete_message(message)
        if everyoneMentionBlock:
            if message.mention_everyone:
                name = str(message.author.name)
                name = name.upper()
                await self.send_message(channel, "COMMUNIST MENTION DETECTED FROM " + name)
                await self.delete_message(message)
                self.violations += 1

        if len(blacklist) != 0 and blacklist != False:
            blacklist = list(await get.Blacklist(message.server))
            text = message.content.strip()
            punc = "*!£$%^&*()_-+={[}]:;@'~#<,>.?/|\`¬\"¡#$%⅋,¿"
            for i in range(len(punc) - 1):
                text = text.replace(punc[i], "")
            text.lower()
            text = text.split(" ")
            for item in blacklist:
                for word in text:
                    if await self.blacklistChecks(word, item, message) == True:
                        return
                    aliases = await get.Alias(item)
                    if aliases != None:
                        aliases = list(aliases)
                        for alias in aliases:
                            if await self.blacklistChecks(word, alias, message) == True:
                                return
        if urlBlock:
            if "https://discord.gg" in msg or "http" in msg or "www." in msg or ".com" in msg or ".org" in msg or ".co.uk" in msg or ".info" in msg or ".ms" in msg:
                if ".png" in msg or ".jpg" in msg or ".jpeg" in msg or ".gif" in msg or ".tiff"in msg:
                    return
                msg = msg.split(" ")
                for item in msg:
                    r = await self.checkURL(item)
                    if r and urlBlock == "true":
                        await self.send_message(channel, "COMMUNIST LINK DETECTED")
                        await self.delete_message(message)
                        self.violations += 1

    async def checkURL(self, url):
        try:
            requests.get(url)
        except:
            return False
        return True

    async def unshorten(self, url):
        try:
            r = requests.head(url, allow_redirects=True)
            return r.url
        except:
            self.errors += 1
            return url

    async def inspire(self, channel):
        phrases = """Communist threat assessment: Minimal. Scanning defenses...
        Global positioning initialized. Location - the Commonwealth of Massachusetts. Birthplace of American freedom
        I hold these truths to be self-evident that all Americans are created... equal. And are endowed with certain unalienable right
        Democracy is the essence of good. Communism, the very definition of evil
        Cultural database accessed. Quoting New England poet Robert Frost: "Freedom lies in being bold"
        Memorial site recognized. Patriotism subroutines engaged. Honoring the fallen is the duty of every red blooded American
        Liberty Prime is online. All systems nominal. Weapons hot. Mission: the destruction of any and all Chinese communists
        Voice module online. Audio functionality test initialized. Designation: Liberty Prime. Mission: the liberation of Anchorage, Alaska
        Obstruction detected, composition: titanium alloy supplemented by photonic resonance barrier. Probability of mission hindrance: zero percent
        Chairman Cheng will fail: China will fall!
        Revised stratagem: Initiate photonic resonance overcharge"""
        phrases = phrases.splitlines()
        phrase = str((phrases[random.randint(0, ((len(phrases)) - 1))]))
        phrase = phrase.upper()
        await self.send_message(channel, phrase)

    async def status(self):
        await self.wait_until_ready()
        while not self.is_closed:

            msg = "**Status**\n\n"

            msg += "*updated at* " + str(strftime("%H:%M:%S", gmtime())) + "\n\n"

            msg += "**name**: " + self.user.name + "\n"
            msg += "**violations**: " + str(self.violations) + "\n"
            msg += "**messages processed**: " + str(self.process) + "\n"
            msg += "**servers**: " + str(len(self.servers)) + "\n"
            number = 0
            unumber = 0
            t = timeit.default_timer()
            for s in self.servers:
                for channel in s.channels:
                    number += 1
                for member in s.members:
                    if not member.bot:
                        unumber += 1
            channel = discord.Object("298168577742340126")
            s = timeit.default_timer()
            await self.send_typing(channel)
            elapsed = timeit.default_timer() - s
            searchtime = timeit.default_timer() - t
            searchtime = searchtime * 1000
            searchtime = "{0:.4f}".format(searchtime)
            elapsed = elapsed * 1000
            elapsed = "{0:.4f}".format(elapsed)
            runtime = timeit.default_timer() - self.boottime
            runtime = str(datetime.timedelta(seconds=int(runtime)))
            msg += "**channels**: {}\n**Users**: {}\n**Errors**: {}\n**Ping**: {}ᵐˢ\n**Runtime**: {}\n**Member len time**: {}ᵐˢ".format(str(number),str(unumber), str(self.errors), elapsed, runtime, searchtime)
            msg = msg.upper()

            try:
                await self.edit_message(self.statusmsg, msg)
            except:
                await self.purge_from(channel)
                self.statusmsg = await self.send_message(channel, msg)
            await asyncio.sleep(30)

    async def openLog(self, server, channel):
        date = strftime("%d-%m")
        if not os.path.exists("ServerLogs"):
            os.mkdir("ServerLogs")
            os.mkdir("ServerLogs/{}".format(str(server.id)))
        if not os.path.exists("ServerLogs/{}".format(str(server.id))):
            os.mkdir("ServerLogs/{}".format(str(server.id)))
        direct = "ServerLogs/{}".format(str(server.id))
        if not os.path.exists("{}/{}".format(direct, channel.name)):
            os.mkdir("{}/{}".format(direct, channel.name))
        if not os.path.isfile("{}/{}/log-{}.txt".format(direct, channel.name, date)):
            f = open("{}/{}/log-{}.txt".format(direct, channel.name, date), "w", encoding='utf-8')
            f.write("===={} LOG FOR {}====".format(server.name, channel.name))
            f.truncate()
            f.close()
        f = open("{}/{}/log-{}.txt".format(direct, channel.name, date), "a", encoding='utf-8')
        return f

    async def logMSG(self, message):
        f = await self.openLog(message.server, message.channel)
        time = strftime("%H:%M:%S")
        date = strftime("%d/%m")
        time = str(date + ", " + time)
        f.write("\n[Time={} Author={}:{}] {}".format(time, str(message.author.name), str(message.author.discriminator), str(message.content)))
        f.truncate()
        f.close()
        try:
            messagelist = list(self.messagedict[message.server.id][message.author.id][message.author.id])
        except:
            messagelist = []
        messagelist.append(message)
        self.messagedict[message.server.id][message.author.id] = {message.author.id: messagelist}

    async def logEvent(self, event, details, server, channel, user=None):
        f = await self.openLog(server, channel)
        time = strftime("%H:%M:%S")
        date = strftime("%d/%m")
        time = str(date + ", " + time)
        msg = ("\nTime={} Action taken:{}  Details:{}".format(time, event, details))
        if user != None:
            try:
                user = user.name #just in case i pass a user object by mistake
            except:
                pass
            msg += ("  User:{}". format(str(user)))
        f.write(msg)
        f.truncate()
        f.close()
        modlog = discord.Object(await get.Modlog(server))
        await self.send_message(modlog, msg.replace("\n", ""))

    async def createAlias(self, word, alias):
        if not os.path.exists("BlacklistAlias"):
            os.mkdir("BlacklistAlias")
        if not os.path.isfile("BlacklistAlias/{}.json".format(word)):
            f = open("BlacklistAlias/{}.json".format(word), "w")
            alias = [alias]
            entry = {'BlacklistWord': word, 'Aliases': alias}
            json.dump(entry, f)
        else:
            f = open("BlacklistAlias/{}.json".format(word), "r+")
            data = json.load(f)
            newalias = list(data['Aliases'])
            if alias in newalias:
                return
            newalias.append(alias)
            data['Aliases'] = newalias
            f.seek(0)
            f.write(json.dumps(data))
            f.truncate()
            f.close()

#
#
#Commands
#
#

    async def handleMute(self, user, server):
        try:
            dir = "data/" + server.id + ".json"
            if not os.path.exists("data"):
                os.mkdir("data")
            if not os.path.isfile(dir):
                return
            else:
                with open(dir, 'r') as r:
                    if None in data['muted']:
                        data['muted'] = str(user.id)
                    else:
                        data['muted'] += " " + str(user.id)
                    r.seek(0)
                    r.write(json.dumps(data))
                    r.truncate()
        except:
            pass


    async def cmd_logdump(self, channel, author, leftover_args, server):
        date = ""
        direct = ""
        if leftover_args:
            if leftover_args == "help":
                await self.send_message(channel, 'YOU CAN GET TODAYS LOG JUST BY TYPING THIS: ``LOGDUMP``\nOR YOU CAN TYPE ``LOGDUMP DAY-MONTH`` FOR ANOTHER DAYS LOG\nE.G. ``LOGDUMP 06-04')
                return
            if "-" in leftover_args[0] or "/" in leftover_args[0]:
                date = str(leftover_args[0])
                date = date.replace("/", "-")
                direct = "ServerLogs/{}/{}/log-{}.txt".format(str(server.id), channel.name, date)
                if not os.path.isfile(direct):
                    await self.send_message(channel, "NO LOG STORED FOR ``{}//{}``".format(channel.name, date))
                    return
        else:
            direct = "ServerLogs/{}/{}/log-{}.txt".format(str(server.id), channel.name, strftime("%d-%m"))
            if date != "":
                date = str(strftime("%d-%m"))
        content = "<@{}> HERE IS THE LOG FOR ``{}`` DATE: ``{}``".format(str(author.id), str(channel.name), date)
        try:
            await self.send_file(channel, fp=direct, filename="{}_{}_log.txt".format(channel.name, strftime("%d-%m")), content=content)
        except Exception as e:
            await self.send_message(channel, "UNABLE TO SEND LOG FILE\n REASON: ``{}``".format(str(e)))

    async def cmd_setup(self, channel):
        log.info("{} has begun its setup".format(str(channel.server.name)))

        ########CREATING FUNCTIONS TO DO REPEATED ACTIONS TO MAKE THINGS CLEANER########
        async def waitForChannel(self, c, a):
            while True:
                setupmsgs.append(await self.send_message(c, "YOU CAN DISABLE THE FUNCTION USING ``DISABLE``"))
                channel = await self.wait_for_message(author=a, channel=c)
                setupmsgs.append(channel)
                if "<#" not in channel.content:
                    if "disable" in channel.content:
                        setupmsgs.append(await self.send_message(c, "FUNCTION DISABLED"))
                        return False
                    else:
                        setupmsgs.append(await self.send_message(c, "MENTION A CHANNEL, OR TYPE ``DISABLE'' TO DISABLE THIS FUNCTION"))
                else:
                    channel = channel.content.strip()
                    channel = channel.replace("<#", "")
                    channel = channel.replace(">", "")
                    return channel

        async def waitForTF(self, c, a):
            while True:
                message = await self.wait_for_message(author=a, channel=c)
                setupmsgs.append(message)
                if "yes" in message.content.lower():
                    return True
                elif "no" in message.content.lower():
                    return False
                else:
                    setupmsgs.append(await self.send_message(c, "``YES`` OR ``NO`` ONLY"))

        ########GETTING REQUIRED STUFF READY########
        global setupmsgs
        setupmsgs = []
        dir = "data/" + channel.server.id + ".json"
        if os.path.isfile(dir):
            os.unlink(dir)
        r = open(dir, "w")
        author = channel.server.owner
        setupmsgs.append(await self.send_message(channel, "**LIBERTY PRIME CONFIGURATION MODE**"))
        await asyncio.sleep(3)

        ########OBTAINING USER SETTINGS########
        #PREFIX#
        setupmsgs.append(await self.send_message(channel, "STATE PREFIX"))
        prefix = await self.wait_for_message(author=author, channel=channel)
        setupmsgs.append(prefix)
        prefix = prefix.content.strip()
        setupmsgs.append(await self.send_message(channel, "PREFIX SET TO ``{}``".format(prefix)))

        #WELCOME CHANNEL#
        setupmsgs.append(await self.send_message(channel, "MENTION YOUR WELCOME CHANNEL"))
        welcome = await waitForChannel(self, channel, author)

        #ANNOUNCEMENT CHANNEL#
        setupmsgs.append(await self.send_message(channel, "MENTION YOUR ANNOUNCEMENT CHANNEL"))
        announcement = await waitForChannel(self, channel, author)

        #MODLOGS#
        setupmsgs.append(await self.send_message(channel, "MENTION A CHANNEL FOR LOGS TO BE SENT TO"))
        logs = await waitForChannel(self, channel, author)

        #MODERATION FREE CHANNEL#
        setupmsgs.append(await self.send_message(channel, "MENTION A CHANNEL TO DISABLE MODERATION IN"))
        free = await waitForChannel(self, channel, author)

        #LINK BLOCKING#
        setupmsgs.append(await self.send_message(channel, "DO YOU WISH TO ACTIVATE LINK BLOCKING"))
        linkBlocking = await waitForTF(self, channel, author)

        #EVERYONE BLOCKING#
        setupmsgs.append(await self.send_message(channel, "DO YOU WISH TO ACTIVATE @EVERYONE BLOCKING"))
        everyoneBlocking = await waitForTF(self, channel, author)

        #BLACKLISTED WORDS#
        setupmsgs.append(await self.send_message(channel, "STATE ANY WORDS YOU WISH TO BLACKLIST\n*seperate each word or phrase with ``, ``*"))
        blacklist = await self.wait_for_message(channel=channel, author=author)
        setupmsgs.append(blacklist)
        blacklist = blacklist.content.strip()
        blacklist = blacklist.lower()
        blacklist = blacklist.split(", ")
        if len(blacklist) != 0:
            setupmsgs.append(await self.send_message(channel, "BLACKLISTING THE FOLLOWING WORDS:\n``{}``".format(str(blacklist))))
        else:
            setupmsgs.append(await self.send_message(channel, "BLACKLIST DISABLED"))

        #SPAM PUNNISHMENT#
        setupmsgs.append(await self.send_message(channel, "SHOULD I KICK, BAN OR MUTE A USER FOR SPAMMING"))
        wait = True
        while wait:
            punnishment = await self.wait_for_message(author=author, channel=channel)
            setupmsgs.append(punnishment)
            if "kick" in punnishment.content.lower():
                punnishment = "kick"
                wait = False
            elif "ban" in punnishment.content.lower():
                punnishment = "ban"
                wait = False
            elif "mute" in punnishment.content.lower():
                punnishment = "mute"
                wait = False
            else:
                setupmsgs.append(await self.send_message(c, "``YES`` OR ``NO`` ONLY"))

        ########SAVING USER SETTINGS########
        try:
            r.seek(0)
            entry = {'prefix': prefix,
                     'urlblock': linkBlocking,
                     'mention': everyoneBlocking,
                     'announcement': announcement,
                     'welcome': welcome,
                     'modrole': None,
                     'modlog':logs,
                     'blacklist':blacklist,
                     'unmoderated':free,
                     'punnishment':punnishment,
                     'muted':None}
            json.dump(entry, r)
            r.truncate()
            r.close()
        except Exception as e:
            m = setupmsg.append(await self.send_message(channel, "CRITICAL ERROR SAVING SERVER CONFIGURATION..."))
            log.critical("{}'S SERVER SETTINGS FAILED TO SAVE\n\n{}".format(channel.server.name, e.__traceback__))
            await self.reportError(e, sys._getframe().f_code.co_name)
            await self.edit_message(m, "CRITICAL ERROR SAVING SERVER CONFIGURATION...\n\nERROR HAS BEEN REPORTED")
            await self.send_message(channel, "JOIN THE SUPPORT SERVER FOR ASSISTANCE")

        await self.send_message(channel, "CONFIGURATION SAVED, RELOADING SERVER")
        m = await self.send_message(channel, "**RELOAD...** PENDING")
        for msg in setupmsgs:
            try:
                await self.delete_message(msg)
            except:
                pass
        await self.edit_message(m, "**RELOAD...** COMPLETE")
        log.info("{} has completed its setup".format(str(channel.server.name)))

    async def cmd_announce(self, author, message):
        if author != message.server.owner:
            await self.send_message(message.channel, "SERVER OWNER ONLY COMMAND")
            return
        channel = await get.Announce(message.server)
        prefix = await get.Prefix(message.server)
        msg = message.content.strip()
        msg = msg.replace((prefix + "announce "), "")

        if message.mention_everyone:
            msg = msg.replace("<@{}>".format(str(message.server.id)), "")
            msg = msg.replace("<@{}>".format(str(message.channel.id)), "")
            msg = msg.replace("@everyone", "")
            msg = msg.replace("@here", "")
        em = discord.Embed(description=msg, colour=(random.randint(0, 16777215)))
        em.set_footer(text=str(author.display_name), icon_url=author.avatar_url)
        await self.send_message(message.channel, ("SENDING ANNOUNCEMENT TO <#" + str(channel.id) + ">"))
        if message.mention_everyone:
            await self.send_message(channel, "@everyone".format(message.server.id), embed = em)
        else:
            await self.send_message(channel, embed=em)

    async def cmd_blacklist(self, channel, leftover_args):
        blacklist = False
        dir = "data/" + channel.server.id + ".json"
        if not os.path.exists("data"):
            os.mkdir("data")
        if not os.path.isfile(dir):
            pass
        else:
            with open(dir, 'r') as r:
                data = json.load(r)
                try:
                    blacklist = str(data['blacklist'])
                except:
                    self.errors += 1
                    blacklist = False
        if blacklist == False or blacklist == "~~None~~":
            log.info("Log was disabled, enabling it now, and using values as new blacklist")
            preserve = False
        else:
            preserve = True

        new = ""
        for item in leftover_args:
            new += (item + ", ")
        if preserve:
            blacklist = blacklist + ", " + new
        else:
            blacklist = new
            blacklist = blacklist[:2]
        with open(dir, 'r+') as r:
            data = json.load(r)
            data['blacklist'] = blacklist
            r.seek(0)
            r.write(json.dumps(data))
            r.truncate()
            log.info(("Updated blacklist for " + channel.server.name))
        await self.send_message(channel, "UPDATED BLACKLIST")

    async def cmd_echo(self, message, channel):
        msg = message
        await self.delete_message(message)
        msg = msg.content.strip()
        msg = msg.replace(await get.Prefix(channel.server), "")
        msg = msg.replace("echo ", "")
        await self.send_message(channel, msg)

    async def cmd_help(self, channel):
        helpmsg = "**COMMANDS**\n```"

        prefix = await get.Prefix(channel.server)
        helpmsg += "cmd_test      : a test command\n"
        helpmsg += "cmd_kick      : kick a mentioned user\n"
        helpmsg += "cmd_ban       : ban a mentioned user\n"
        helpmsg += "cmd_clean     : remove up all bot messages\n"
        helpmsg += "cmd_nuke      : reset the channel\n"
        helpmsg += "cmd_welcome   : change which channel welcome messages go\n"
        helpmsg += "cmd_prefix    : change the prefix\n"
        helpmsg += "cmd_url       : enable or disable link blocking\n"
        helpmsg += "cmd_logdump   : dumps the message log for that channel\n"
        helpmsg += "cmd_announce  : send an announcement to your chosen channel\n"
        helpmsg += "```"
        helpmsg = helpmsg.replace("cmd_", prefix)
        helpmsg = helpmsg.upper()
        await self.send_message(channel, helpmsg)

    async def cmd_test(self, server, channel, author):
        await self.send_message(channel, "TEST MESSAGE")
        await self.send_message(channel, ("CHANNEL NAME: " + channel.name))
        await self.send_message(channel, ("SERVER NAME: " + server.name))
        await self.send_message(channel, ("USER NAME: " + author.name))

    async def cmd_kick(self, channel, user_mentions):
        if len(user_mentions) == 0:
            await self.send_message(channel, "YOU DID NOT SPECIFY ANY USERS")
        elif self.user in user_mentions:
            await self.send_message(channel, "{} IS IN THE KICK LIST, TERMINATING SERVER OPERATIONS".format(self.user.name.upper()))
        else:
            for user in user_mentions:
                try:
                    msg = await self.send_message(channel, "REMOVING {}...".format(user.display_name.upper()))
                    await self.kick(user)
                    await self.edit_message(msg, "REMOVED {}".format(user.display_name.upper()))
                except Exception as e:
                    self.errors += 1
                    log.error(e)
                    await self.send_message(channel, "FAILED TO BAN COMMUNIST: {}".format(user.name))

    async def cmd_ban(self, channel, user_mentions):
        if len(user_mentions) == 0:
            await self.send_message(channel, "YOU DID NOT SPECIFY ANY USERS")
        elif self.user in user_mentions:
            await self.send_message(channel, "{} IS IN THE BAN LIST, TERMINATING SERVER OPERATIONS".format(self.user.name.upper()))
        else:
            for user in user_mentions:
                try:
                    msg = await self.send_message(channel, "BANNING {}...".format(user.display_name.upper()))
                    await self.ban(user)
                    await self.edit_message(msg, "BANNED {}".format(user.display_name.upper()))
                except Exception as e:
                    self.errors += 1
                    log.error(e)
                    await self.send_message(channel, "FAILED TO BAN COMMUNIST: {}".format(user.name))

    async def cmd_clean(self, channel):
        self.send_typing(channel)
        search_range = 1500
        deleted = int(0)
        prefix = await get.Prefix(channel.server)
        async for entry in self.logs_from(channel, search_range):
            self.send_typing(channel)
            try:
                if entry.content.startswith(self.prefix):
                    await self.delete_message(entry)
                    deleted += 1
                elif entry.author == self.user:
                    await self.delete_message(entry)
                    deleted += 1
                elif "<@277395792535355392>" in entry.content:
                    await self.delete_message(entry)
                    deleted += 1
                elif entry.content.startswith(prefix):
                    await self.delete_message(entry)
                    deleted += 1
            except discord.errors.NotFound:
                self.errors += 1
                pass
        await self.send_message(channel, ("Deleted " + str(deleted) + " messages"))

    async def cmd_nuke(self, channel):
        search_range = 999999999999999
        deleted = int(0)
        try:
            await self.purge_from(channel, limit=search_range)
        except:
            async for entry in self.logs_from(channel, search_range):
                deleted += 1
                await self.delete_message(entry)

    async def cmd_alert(self, channel, leftover_args):
        message = "DEV MESSAGE: "
        async for item in leftover_args():
            message += " " + item
        await self.send_message(channel, ("SENDING DEV MESSAGE TO " + str(len(self.servers))))
        number = int(0)
        async for s in self.servers:
            try:
                await self.send_message(s, message)
                number += 1
                if number % 25 == 0:
                    if number != len(self.servers):
                        await self.send_message(channel, (str(number) + " SENT"))
            except discord.Forbidden:
                self.errors += 1
                await self.send_message(channel(s.name + " BLOCKED ME FROM SENDING"))
        await self.send_message(channel, ("SENT " + number + " DEV MESSAGES"))

    async def cmd_welcome(self, channel_mentions, message, server):
        dir = "data/" + server.id + ".json"
        for channel in channel_mentions:
            if not os.path.exists("data"):
                os.mkdir("data")
            if not os.path.isfile(dir):
                with open(dir, 'w') as r:
                    entry = {'welcome': channel.id, 'prefix': "L-", 'urlblock': 'True', 'mention': 'False',
                             'announcement': 'None', 'modrole': 'None', 'modlog': 'None'}
                    json.dump(entry, r)
            else:
                with open(dir, 'r+') as r:
                    data = json.load(r)
                    data['welcome'] = channel.id
                    r.seek(0)
                    r.write(json.dumps(data))
                    r.truncate()
            await self.send_message(message.channel, "Set new welcome channel")

    async def cmd_prefix(self, leftover_args, server, channel):
        dir = "data/" + server.id + ".json"
        if not os.path.exists("data"):
            os.mkdir("data")
        if not os.path.isfile(dir):
            with open(dir, 'w') as r:
                entry = {'welcome': channel.id, 'prefix': "L-", 'urlblock': 'True', 'mention': 'False',
                         'announcement': 'None', 'modrole': 'None', 'modlog': 'None'}
                json.dump(entry, r)
        else:
            with open(dir, 'r+') as r:
                data = json.load(r)
                try:
                    data['prefix'] = str(leftover_args[0])
                except:
                    await self.send_message(channel, "CURRENT PREFIX: ``{}``\nTO SET A NEW ONE, USE ``{}PREFIX [NEW PREFIX]``".format(data['prefix'], data['prefix']))
                    return
                r.seek(0)
                r.write(json.dumps(data))
                r.truncate()
        await self.send_message(channel, "Set prefix to ``" + str(leftover_args[0]) + "``")

    async def cmd_url(self, leftover_args, server, channel):
        try:
            result = str(leftover_args[0])
        except:
            await self.send_message(channel, "ERROR, SYSTEM REQUIRES A CONDITION.\nUSE ``URL ENABLE`` OR ``URL DISABLE`` ")
            return
        result = result.lower()
        if result == "enable":
            result = "True"
        elif result == "disable":
            result = "False"
        else:
            await self.send_message(channel, "COMMUNIST RESPONSE DETECTED\n\nUSE ``/URL ENABLE`` OR ``/URL DISABLE``")
        dir = "data/" + server.id + ".json"
        if not os.path.exists("data"):
            os.mkdir("data")
        if not os.path.isfile(dir):
            with open(dir, 'w') as r:
                entry = {'welcome': channel.id, 'prefix': "L-", 'urlblock': str(result)}
                json.dump(entry, r)
        else:
            with open(dir, 'r+') as r:
                data = json.load(r)
                data['urlblock'] = str(result)
                r.seek(0)
                r.write(json.dumps(data))
                r.truncate()
        if result == "False":
            await self.send_message(channel, "RED ARMY LINK BLOCKING DISABLED")
        elif result == "True":
            await self.send_message(channel, "DESTROYING ALL RED ARMY LINKS")

    async def cmd_alias(self, leftover_args, author, server, channel):
        blacklist = leftover_args[0].lower()
        alias = leftover_args[1]
        if author.id != "174918559539920897":
            try:
                await self.send_message(discord.Object("301010223257223169"), "**Alias Submission:**\n{} == {}".format(blacklist, alias))
            except Exception as e:
                await self.reportError(e, sys._getframe().f_code.co_name)
                await self.send_message(channel, "AN ERROR HAS OCCURED SUBMITTING YOUR ALIAS, DNA HAS BEEN NOTIFIED")
            await self.send_message(channel, "{} HAS BEEN SUBMITTED AS AN ALIAS FOR {}".format(alias, blacklist))
        else:
            await self.createAlias(blacklist, alias)
            await self.send_message(channel, "**{}** HAS BEEN ADDED AS AN ALIAS FOR **{}**".format(alias.upper, blacklist.upper))


#
#
# Events

    async def cmd_mute(self, leftover_args, server, channel):
        try:
            if "<@" not in leftover_args[1]:
                await self.send_message(channel, "SPECIFY A USER TO MUTE ``{}MUTE [ACTION] [USER]``".format(await get.Prefix(server)))
                return
            else:
                user = leftover_args[1]
                user = user.replace("<@", "")
                user = user.repalce(">", "")
        except:
            await self.send_message(channel, "INCORRECT FORMATTING... ``{}MUTE [ACTION] [USER]``".format(await get.Prefix(server)))
            return
        action = leftover_args[0]
        if action.lower() == "add":
            await self.handleMute(user, server)
            await self.send_message(channel, "MUTED")
        elif action.lower() == "remove":
            await self.handleUnmute(user, server)
        else:
            await self.send_message(channel, "SPECIFY AN ACTION, ADD OR REMOVE")


    async def on_message(self, message):
        await self.wait_until_ready()
        message_content = message.content.strip()
        prefix = await get.Prefix(message.server)
        await self.logMSG(message)
        if not message_content.startswith(prefix):
            await self.checkMSG(message)
            return
        if message.author == self.user:
            return
        try:
            if message.author.id in await get.Mute(message.server):
                await self.delete_message(message)
        except:
            pass
        if message.author != message.server.owner:
            if message.server.id != "277423067288829952":
                for role in message.author.roles:
                    if role.name.lower() == "no communism":
                        break
                    else:
                        return

        command, *args = message_content.split(
            ' ')  # Uh, doesn't this break prefixes with spaces in them (it doesn't, config parser already breaks them)
        command = command[len(prefix):].lower().strip()
        handler = getattr(self, 'cmd_' + command, None)
        if not handler:
            return
        log.info("{0.id}/{0!s}: {1}".format(message.author, message_content.replace('\n', '\n... ')))
        argspec = inspect.signature(handler)
        params = argspec.parameters.copy()
        # noinspection PyBroadException
        try:
            handler_kwargs = {}
            if params.pop('message', None):
                handler_kwargs['message'] = message
            if params.pop('channel', None):
                handler_kwargs['channel'] = message.channel
            if params.pop('author', None):
                handler_kwargs['author'] = message.author
            if params.pop('server', None):
                handler_kwargs['server'] = message.server
            if params.pop('leftover_args', None):
                handler_kwargs['leftover_args'] = args
            args_expected = []
            for key, param in list(params.items()):
                # parse (*args) as a list of args
                if param.kind == param.VAR_POSITIONAL:
                    handler_kwargs[key] = args
                    params.pop(key)
                    continue
                # parse (*, args) as args rejoined as a string
                # multiple of these arguments will have the same value
                if param.kind == param.KEYWORD_ONLY and param.default == param.empty:
                    handler_kwargs[key] = ' '.join(args)
                    params.pop(key)
                    continue
                doc_key = '[{}={}]'.format(key, param.default) if param.default is not param.empty else key
                args_expected.append(doc_key)
                # Ignore keyword args with default values when the command had no arguments
                if not args and param.default is not param.empty:
                    params.pop(key)
                    continue
                # Assign given values to positional arguments
                if args:
                    arg_value = args.pop(0)
                    handler_kwargs[key] = arg_value
                    params.pop(key)
            # Invalid usage, return docstring
            if params:
                docs = getattr(handler, '__doc__', None)
                if not docs:
                    docs = 'Usage: {}{} {}'.format(
                        prefix,
                        command,
                        ' '.join(args_expected)
                    )
                docs = dedent(docs)
                await self.safe_send_message(
                    message.channel,
                    '```\n{}\n```'.format(docs.format(command_prefix=prefix)),
                    expire_in=60
                )
                return
            response = await handler(**handler_kwargs)
            if response and isinstance(response, Response):
                content = response.content
                if response.reply:
                    content = '{}, {}'.format(message.author.mention, content)
                await self.safe_send_message(
                    message.channel, content,
                    expire_in=response.delete_after if self.config.delete_messages else 0,
                    also_delete=message if self.config.delete_invoking else None
                )
        except Exception:
            self.errors += 1
            log.error("Exception in on_message", exc_info=True)

    async def on_server_join(self, server):
        log.info(("Joined " + server.name))
        log.info("searching for usable channel")
        target = server
        for channel in server.channels:
            if channel.type == "text":
                name = channel.name
                name = name.lower()
                if name == "general":
                    target = channel
                    break
                elif name == "public":
                    target = channel
                    break
                elif name == "discussion":
                    target = channel
                    break
                if name == "lobby":
                    target = channel
                    break
        log.info(("sending join message to " + target.name))
        await self.send_message(target, "LIBERTY PRIME IS ONLINE")
        await self.send_message(target, "ALL SYSTEMS NOMINAL")
        await self.send_message(target, "WEAPONS HOT")
        await self.send_message(target, "MISSION: THE DESTRUCTION OF ANY AND ALL CHINESE COMMUNISTS")
        await asyncio.sleep(0.3)
        await self.send_message(target, "COMMUNISM REMOVAL SUBROUTINES ENGAGED")
        await asyncio.sleep(1)
        await self.cmd_setup(target)

    async def on_server_remove(self, server):
        log.info(("COMMUNISM OVERTHREW " + server.name))
        log.info("PURGING SERVER")
        os.unlink("data/{}.json".format(str(server.id)))
        os.unlink("ServerLogs/{}".format(str(server.id)))
        log.info("Server information deleted")

    async def on_member_join(self, member):
        server = member.server
        try:
            channel = await get.Welcome(server)
            name = member.name
            name = name.upper()
            if member.id == "174918559539920897":
                await self.send_message(channel, "<@174918559539920897> HAS JOINED THE SERVER, SWITCHING TO ``DEBUG MODE``. COMMUNISM PREVENTION MODE ``PRIORITY``")
            await self.send_message(channel, (name + " JOINED THE SERVER. SCANNING FOR COMMUNISM"))
            if member.id in self.spammers:
               await self.send_message("COMMUNISM DETECTED. USER {} HAS BEEN MARKED AS A SPAMMER".format(member.name))
               await self.kick(member)
               return
            await self.send_message(channel, (name + " IS CLEAR OF ALL COMMUNISM"))
            if member.bot:
                await self.send_message(channel, "WARNING, " + name + " ISNT AN ORGANIC")
        except discord.errors.Forbidden:
            self.errors += 1
            pass

    async def on_member_remove(self, member):
        try:
            channel = await get.Welcome(member.server)
            name = member.name
            name = name.upper()
            await self.send_message(channel, (name + " LEFT THE SERVER. DAMN COMMUNISTS"))
        except Exception as e:
            self.errors += 1
            log.error(e)
            pass

    async def on_ready(self):
        import os
        self.boottime = float(timeit.default_timer())
        self.owner = await self.application_info()
        self.owner = self.owner.owner
        log.info('LIBERTY PRIME IS ONLINE')
        log.info('ALL SYSTEMS NOMINAL')
        log.info('WEAPONS HOT')
        log.info('MISSION: THE DESTRUCTION OF ANY AND ALL CHINESE COMMUNISTS')
        log.info(str('DESIGNATION: ' + self.user.name))
        log.info(str('IDENTIFICATION: ' + self.user.id))
        log.info(str('PREFIX: ' + self.prefix))
        log.info(str('Owner: ' + self.owner.name))
        #await self.send_message(discord.Object("277423067288829952"), "**LIBERTY PRIME IS ONLINE, ALL SYSTEMS NOMINAL...**")
        if len(self.servers) == 0:
            log.info("im not in any servers")
        log.info("Server List:")
        for s in self.servers:
            log.info(("    " + s.name))
            dir = "data/" + s.id + ".json"
            if not os.path.exists("data"):
                os.mkdir("data")
            if not os.path.isfile(dir):
                with open(dir, 'w') as r:
                    entry = {'welcome': s.id, 'prefix': "L-", 'urlblock': 'True', 'mention': 'False',
                             'announcement': 'None', 'modrole': 'None', 'modlog': 'None'}
                    json.dump(entry, r)
        self.wait_until_ready()
        self.loop.create_task(self.status())
        try:
            await self.change_presence(game=discord.Game(name='WITH COMMUNISM'))
        except Exception as e:
            log.error(e)